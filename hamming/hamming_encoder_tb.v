`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Testbench for module: hamming_encoder
//
// Mohammad Rakib
// M. Engg. ICT, IICT
////////////////////////////////////////////////////////////////////////////////

module hamming_encoder_tb;

	// Inputs
	reg CLK;
	reg nRST;
	reg [0:33] in;
	reg en;

	// Outputs
	reg [1:58] out; // 7*8+2=58

	// Instantiate the Unit Under Test (UUT)
	hamming_encoder uut (
		.CLK(CLK), 
		.nRST(nRST), 
		.en(en),
		.in(in),
		.out(out)
	);

	initial begin
		// Initialize Inputs
		CLK = 0;
		nRST = 0;
		in= 34'h0563ac3af;
		en=1;

		// Wait 10 ns for global reset to finish
		#10;
        nRST = 1;
	end
	parameter DELAY = 1;
	always 
		# DELAY CLK = ~CLK;
      
endmodule

